
.PHONY: test-results
test: test-results

TESTS=$(notdir $(wildcard tests/*))
test-results:
	rm -rf $@
	mkdir $@
	$(foreach t,$(TESTS),\
		python darcs2git.py --author authors.test --debug -d $@/$(t).git tests/$(t) \
		&& ) true

show:
	$(foreach t,$(TESTS),\
		GIT_DIR=test-results/$(t).git gitk & ) true
